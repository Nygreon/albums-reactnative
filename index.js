// Import a library to help create component
import React from 'react';
import { AppRegistry, SafeAreaView } from 'react-native';
import Header from './src/components/header';
import AlbumList from './src/components/AlbumList';


// Create component
const App = () => (
    <SafeAreaView style={{ flex: 1 }}>
        <Header headerText={'Albums'} />
        <AlbumList />
    </SafeAreaView>
    );

// Render
AppRegistry.registerComponent('albums', () => App);
